const express = require('express');
const router = express.Router();

router.get('/', function (req, res) {
    res.send('API works!');
});

router.get('/test', function (req, res) {
    res.send('TEST works!');
});


module.exports = router;